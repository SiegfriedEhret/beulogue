---
title: Contenu
date: 2019-06-29
description: Tous sur les contenus dans beulogue
weight: 2.0
---

Les contenus doivent se trouver dans le dossier `content` de votre site. Le format attendu est [markdown](https://fr.wikipedia.org/wiki/Markdown).

Les fichiers de sortie auront le même nom que vos fichiers markdown. Ils se terminent par `.html` au lieu de `.md`, bien entendu.

Vous pouvez injecter du contenu dans vos modèles de page
You can inject content in your list template using a `_index.md` (for your content language) or `_index.LANG.md` for a given `LANG` (example: `_index.fr.md` for french).

[⬅️ configuration](/fr/usage/configuration.html) || [matière liminaire ➡️](/fr/content/front-matter.html)