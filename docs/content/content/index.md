---
title: Content
date: 2019-05-25
description: All about contents in beulogue
weight: 2.0
---

Content items must be in the `content` folder of your site. The expected format is [markdown](https://en.wikipedia.org/wiki/Markdown).

The output files will have the same name as your markdown file. The filenames end with `.html` instead of `.md`, of course.

You can inject content in your list template using a `_index.md` (for your content language) or `_index.LANG.md` for a given `LANG` (example: `_index.fr.md` for french).

[⬅️ configuration](/en/usage/configuration.html) || [front matter ➡️](/en/content/front-matter.html)