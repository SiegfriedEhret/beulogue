---
title: Installation
date: 2019-10-14
description: Comment installer beulogue
weight: 1.1
---

## Télécharger les binaires

- Linux: :white_check_mark:
- MacOS: :construction: (les versions MacOS arriveront toujours en décalé)
- Windows: :x:

Allez à la page des [livraisons](https://github.com/SiegfriedEhret/beulogue/releases) !

## Construire depuis les sources

- Clonez ce dépôt.
- Installez [Crystal](https://crystal-lang.org/) et [Shards](https://github.com/crystal-lang/shards).
- Lancez `make install` puis `make`.
- Le binaire `beulogue` sera dans le répertoire `bin/`.

[⬅️ accueil](/fr/) || [utilisation ➡️](/fr/usage/usage.html)