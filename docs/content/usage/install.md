---
title: Installation
date: 2019-05-26
description: How to install beulogue
weight: 1.1
---

## Download binaries

- Linux: :white_check_mark:
- MacOS: :construction: (MacOS versions will always come later than the Linux one)
- Windows: :x:

Go to the [release page](https://github.com/SiegfriedEhret/beulogue/releases) !

## Build from source

- Clone this repository.
- Install [Crystal](https://crystal-lang.org/) and [Shards](https://github.com/crystal-lang/shards).
- Run `make install` then `make`.
- The `beulogue` binary will be in the `bin/` folder.

[⬅️ home](/en/) || [usage ➡️](/en/usage/usage.html)