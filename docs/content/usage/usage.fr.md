---
title: Utilisation
date: 2019-10-14
description: Utilisation de beulogue
weight: 1.2
---

Ajoutez `beulogue` à votre `PATH` puis lancez `beulogue` !

Les commandes disponibles sont:

- `beulogue` ou `beulogue build` pour construire le site.
- `beulogue help` pour afficher l'aide.
- `beulogue version` pour afficher la version.

Drapeaux:

- `-dev` or `--development`: 
  - pour utiliser une `base` vide au lieu de celle indiquée dans le fichier de configuration pour rendre la navigation plus facile en local
  - pour prendre en compte les fichiers dans le répertoire `drafts/`
- `-d` or `--debug`: pour afficher quelques messages
- `-h` or `--help`: pour afficher l'aide

Protip: some basic templates just for you ! Please go to:

- [bitbucket](https://bitbucket.org/siegfriedehret/beulogue-templates/)
- [codeberg](https://codeberg.org/SiegfriedEhret/beulogue-templates/)
- [github](https://github.com/SiegfriedEhret/beulogue-templates/)

[⬅️ installation](/fr/usage/install.html) || [structure ➡️](/fr/usage/structure.html)