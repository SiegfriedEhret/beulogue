---
title: Usage
date: 2019-05-26
description: How to run beulogue
weight: 1.2
---

Add `beulogue` to your `PATH` and run `beulogue` !

Here are the possible commands:

- `beulogue` or `beulogue build` to build the site.
- `beulogue help` to show the help.
- `beulogue version` to show the the version.

Possible flags:

- `-dev` or `--development`: 
  - will use an empty `base` instead of the one given in the configuration file to make local navigation easier
  - will add markdown in the `drafts/` folder
- `-d` or `--debug`: show some logs
- `-h` or `--help`: show help

Protip: voici quelques kits de démarrage juste pour vous ! Rendez-vous sur l'un de ces miroirs:

- [bitbucket](https://bitbucket.org/siegfriedehret/beulogue-templates/)
- [codeberg](https://codeberg.org/SiegfriedEhret/beulogue-templates/)
- [github](https://github.com/SiegfriedEhret/beulogue-templates/)

[⬅️ install](/en/usage/install.html) || [structure ➡️](/en/usage/structure.html)